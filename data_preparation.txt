export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim

1.  Запускаем label image:
    cd work/alexey/programms/labelImg
    python3 labelImg.py

2.  Открываем папку с картинками для разметки(значек слева Open Dir)

3.  Нажимаем на кнопку слева снизу CreateRectBox и далее выделяем интерисующую область

4.  После выделения выбираем класс объекта.
    Далее проводим те же манипуляции для всех объектов на картинке.

5.  Сохраняем xml файл Ctrl + S и переходим к следующей картинке путем нажатия кнопки
    Next Image слева.

6.  Создаем дерево папок:
    Object_detection
      |-images		# здесь хранятся все фото
      |   |-test	# здесь xml
      |   |-train	# здесь xml
      |-data
      |-training
	  |-model_folders # как пример это faster_rcnn_... ssd_mobilent и т.д
6.  Помещаем все картинки c xml файломи в папки: test - 10%, train - 90%

7.  Переходим https://github.com/datitran/raccoon_dataset
    Создаем в Object_detection файл xml_to_csv.py. Копируем содержимое с gitHub и правим под наш проект:
    Заменяем функцию main:
    def main():
    for directory in ['train', 'test']:
        image_path = os.path.join(os.getcwd(), 'images/{}'.format(directory))
        xml_df = xml_to_csv(image_pa rth)
        xml_df.to_csv('data/{}_labels.csv'.format(directory), index=None)
        print('Successfully converted xml to csv.')

8.	Создаем файлы для тренировки. Копируем с git про енота файл genetate_tfrecord.py
    В файле 20 строчка подсвечивает from object_detection.utils import dataset_util
    Необходимо в папке с Tensorflow Object Detection Api:
    # model/reserch
    sudo python3 setup.py install
    Редактируем 31 строку под свой класс if row_label == 'plate_number': . Если классов много, то
    делаем через elif. Функция не должна возвращать None.
    Редактируем 5 строку
    python generate_tfrecord.py --csv_input=data/train_labels.csv  --output_path=data/train.record --image_dir=images
    Then:
    python generate_tfrecord.py --csv_input=data/test_labels.csv  --output_path=data/test.record --image_dir=images
		
9.	Вибираем модель и конфиг файл чтобы они совпадали
    Модель:
    https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/detection_model_zoo.md
    Конфиг файл:
    https://github.com/tensorflow/models/tree/master/research/object_detection/samples/configs
    В начале конфиг файла правим количество классов num_classes
    train_config { fine_tune_checkpoint: заменяем на "name_directory_with_model.ckpt"
    train_input_reader: { tf_record_input_reader { input_path: "data/train.record"
    train_input_reader: { label_map_path: "data/object-detection.pbtxt"
    eval_input_reader: { tf_record_input_reader { input_path: "data/test.record"
    eval_input_reader: { label_map_path: "data/object-detection.pbtxt"

	ВНИМАНИЕ: посмотреть максимальное количество шагов обучения(может надо изменить)
	num_steps: 200000

10. Create new file in directory(data, training) - 'object-detection.pbtxt'
    In file write:
    item {
        id: 1
        name: 'plate_number'
    }

11. Copy our folder object-detection to models/research/object_detection
    Спросят совмещать папки data - отвечаем да
    Перемещаем конфиг файл, который правили name_model.config в папку training


https://becominghuman.ai/tensorflow-object-detection-api-tutorial-training-and-evaluating-custom-object-detector-ed2594afcf73

	запустить тренировку	
	python train.py --logtostderr --train_dir=training/ --pipeline_config_path=training/faster_rcnn_inception_v2_coco.config
	python model_main.py --logtostderr --train_dir=training/ --pipeline_config_path=training/faster_rcnn_inception_v2_coco.config
	

12. Check learning process:
    From models/object_detection, via terminal, you start TensorBoard with:
    tensorboard --logdir='training'

			export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
12.  python export_inference_graph.py \
    --input_type image_tensor \
    --pipeline_config_path training/faster_rcnn_inception_v2_coco.config \
    --trained_checkpoint_prefix /home/user/work/lib/tensorflow/pretrain_models/faster_rcnn_intersection_two_classes_20032019/training_20032019/model.ckpt-350000 \
    --output_directory /home/user/work/lib/tensorflow/pretrain_models/faster_rcnn_intersection_two_classes_20032019


# импортируем Pandas и Numpy
import pandas as pd
import numpy as np

df = pd.read_csv('../../data/telecom_churn.csv')
df['class'].value_counts()

# посмотреть колонки
print(df.columns)

# посмотреть общую информацию
print(df.info())

df[df['class'] == 'A'].head()

# перемещенние группы файлов
mv ./train/000{010..20}.xml ./test/

# создать лист названий вайлов с классом child
file_name_child = list(df[df['class']=='child'].filename)







    
